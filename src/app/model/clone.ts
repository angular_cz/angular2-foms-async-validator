import { User } from './user';
export const cloneUser = (user: User) : User => {
  const newUser = Object.assign({}, user);
  newUser.contact = Object.assign({}, user.contact);

  return newUser;
};
