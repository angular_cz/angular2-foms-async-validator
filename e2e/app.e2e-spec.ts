import { AdvancedFormsPage } from './app.po';

describe('advanced-forms App', function() {
  let page: AdvancedFormsPage;

  beforeEach(() => {
    page = new AdvancedFormsPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
